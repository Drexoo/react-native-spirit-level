import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { Accelerometer } from 'expo-sensors';

export default function App() {
  const [data, setData] = useState({
    x: 0,
    y: 0,
    z: 0
  });

  const [subscription, setSubscription] = useState(null);
  let button;

  const acc = () => {
    setSubscription(
      Accelerometer.addListener(accelerometerData => {
        setData(accelerometerData);
        Accelerometer.setUpdateInterval(150);
      })
    );
  };

  useEffect(() => {
    acc();
  }, []);

  const { x, y, z } = data;

  return (
    <View style={((x > -0.05 && x < 0.05 && y > -0.05 && y < 0.05) || (x > -0.05 && x < 0.05 && y > 0.95) || (y > -0.05 && y < 0.05 && x > 0.95) || (y > -0.05 && y < 0.05 && x < -0.95) ? styles.show : styles.container)}>
      <Text style={styles.text}>
        {(x > -0.05 && x < 0.05 && y > -0.05 && y < 0.05 ? 'Jest równo': '')}
        {(x > -0.05 && x < 0.05 && y > 0.95 ? 'Jest równo w pionie': '')}
        {((y > -0.05 && y < 0.05 && x > 0.95) || (y > -0.05 && y < 0.05 && x < -0.95) ? 'Jest równo w poziomie': '')}
        {((x > 0.05 && x < 0.95) || (y > 0.05 && y < 0.95) || (x < -0.05 && x > -0.95) || (y < -0.05 && y > -0.95) ? 'Nie jest równo': '')}
      </Text>
      <Text>
        x: {round(x)}{"\n"}y: {round(y)}{"\n"}z: {round(z)}{"\n"}
      </Text>
    </View>
  );
}

function round(n) {
  if (!n) {
    return 0;
  }
  return Math.floor(n * 100) / 100;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  show: {
    backgroundColor: 'green',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  text: {
    paddingBottom: 20,
    fontSize: 30
  }
});